# ICeChIP-seq in xenopus laevis

This repository reports the steps followed to process IceChIP-seq performed on Xenopus laevis sperm samples. 

Two different histone modifications have been studied, H3K4me3 and H3K27me3. Both pull-down and inputs samples have been sequences together with the add of external spike sequencing codifying control semi-synthetic nucleosomes carrying various level of signals. Those are used for internal calibration of the ChIP-seq signal and allow to compute HMD (Histone-modification density).

## Getting Started

These instructions will get you a copy of the project and part of the scripts used at different step of the analysis.

### Prerequisites and genome version
Genome version and annotation

* Xenopus Laevis 6.1

* ladder sequences - provided by those generating the semi-synthetic nucleosomes
* annotation file


### Tools

Tools used at various steps of the analysis:

* cutadapt
* Picard 2.14
* BWA-mem
* samtools
* bedtools
* deeptools
* macs2
*

Ad-hoc and customized scripts in R and bash have been used on local machines as well on a distributed systems (SLURM, LSF) to perform parallelised computations.



### Sequencing kit used for immuno-precipitation

* TruSeq DNA kit (Illumina, FC-121- 2001).


### General overview of the processing of the ICE-ChIP-seq data

* General description of the processing can be found [here, Ice_ChIP_general_processing](./Ice_ChIP_general_Processing.md)

## Authors

* **Angela Simeone** - ** - [angela](https://github.com/angsim)

