Xenopus processing of input samples
================
Angela Simeone

General outline
---------------

Processing of ChIP-seq input data (paired-end). This can be done after pre-processing the initial data as reported [here](./Ice_ChIP_general_Processing.md).

In short, fragment from the input sample are separated base on size. For each fragmnent size we compute the global frequency of each of the 3 fragment sized considered in the study (150, 110, 70).

Those global frequencies are used to estimate locally (in 50-bp windows spanning the genome) the probabiliy of each of the 3 fragments occurring.

Those probabilities are then used to feed the HMM that classify the windows as enriched or not enriched for the specific fragment-size individually. Therefore for each fragment-size, an HMM has been used.

Regions of the genome with less than 5 reads are excluded from the analysis.

INPUT sample processing - for nucleosomal and sub-nucleosomal particle identification
-------------------------------------------------------------------------------------

``` bash
# compute coverage input for 150-110-70 across the genome binned in 250bp bins sliding 50pb

#--list inputs.txt
## without duplicates
# Mami.Index4.mergedbam.paired_reads.Fragm150.sorted.bed
# Mami.Index4.mergedbam.paired_reads.Fragm110.sorted.bed
# Mami.Index4.mergedbam.paired_reads.Fragm70.sorted.bed
# Mami.Index12.mergedbam.paired_reads.Fragm150.sorted.bed
# Mami.Index12.mergedbam.paired_reads.Fragm110.sorted.bed
# Mami.Index12.mergedbam.paired_reads.Fragm70.sorted.bed
## with duplicates
# Mami.Index4.merged.allreads.paired_reads.sorted.fixed150.bed
# Mami.Index4.merged.allreads.paired_reads.sorted.fixed110.bed
# Mami.Index4.merged.allreads.paired_reads.sorted.fixed70.bed
# Mami.Index12.merged.allreads.paired_reads.sorted.fixed150.bed
# Mami.Index12.merged.allreads.paired_reads.sorted.fixed110.bed
# Mami.Index12.merged.allreads.paired_reads.sorted.fixed70.bed


# compute coverage 
windowfile='/mnt/home1/gurdon/as2363/Laevis6_1-regions/LAEVIS_6.1greaterthan10k_250bpwindows_slid50.bed'
output_dir=/mnt/home1/gurdon/as2363/Mami_Oct_2017/Proc_Data/Input_coverage
while read filename;
do
sbatch -p IACT -e tmp_cov_in.err -o tmp_tmp_cov_in.out -N 1 -n 8 --wrap="bedtools coverage -a $windowfile -b $filename > $output_dir/${filename/.bed/.250bpw_slid50.bed}"
done < list_inputs.txt

while read filename;
do
wc -l $filename
done < list_inputs.txt


#sum inputs - by summing coverage in the same window - sliding window 250 wide - 50 shift
paste Mami.Index4.merged.allreads.paired_reads.sorted.fixed150.250bpw_slid50.bed Mami.Index12.merged.allreads.paired_reads.sorted.fixed150.250bpw_slid50.bed | awk '{print$1"\t"$2"\t"$3"\t"($4+8)}' > Input_allreads_Frag150.250bpw_slid50.bed &&
paste Mami.Index4.merged.allreads.paired_reads.sorted.fixed110.250bpw_slid50.bed Mami.Index12.merged.allreads.paired_reads.sorted.fixed110.250bpw_slid50.bed | awk '{print$1"\t"$2"\t"$3"\t"($4+8)}' > Input_allreads_Frag110.250bpw_slid50.bed &&
paste Mami.Index4.merged.allreads.paired_reads.sorted.fixed70.250bpw_slid50.bed Mami.Index12.merged.allreads.paired_reads.sorted.fixed70.250bpw_slid50.bed | awk '{print$1"\t"$2"\t"$3"\t"($4+8)}' > Input_allreads_Frag70.250bpw_slid50.bed

paste Mami.Index4.mergedbam.paired_reads.Fragm150.sorted.250bpw_slid50.bed Mami.Index12.mergedbam.paired_reads.Fragm150.sorted.250bpw_slid50.bed | awk '{print$1"\t"$2"\t"$3"\t"($4+8)}' > Input_nodup_Frag150.250bpw_slid50.bed &&
paste Mami.Index4.mergedbam.paired_reads.Fragm110.sorted.250bpw_slid50.bed Mami.Index12.mergedbam.paired_reads.Fragm110.sorted.250bpw_slid50.bed | awk '{print$1"\t"$2"\t"$3"\t"($4+8)}' > Input_nodup_Frag110.250bpw_slid50.bed &&
paste Mami.Index4.mergedbam.paired_reads.Fragm70.sorted.250bpw_slid50.bed Mami.Index12.mergedbam.paired_reads.Fragm70.sorted.250bpw_slid50.bed | awk '{print$1"\t"$2"\t"$3"\t"($4+8)}' > Input_nodup_Frag70.250bpw_slid50.bed
```

### Processing INPUT samples to get data formatted for next step - i.e. extract probabilities for HMM out of frequecies

``` bash

## case 1 - create bigwig out the coverage of input =============================================================
## Input .250bpw_slid50.bed

for file in I*.250bpw_slid50.bed
    do
    sbatch -p IACT -e tmp_cov_in.err -o tmp_tmp_cov_in.out -N 1 -n 8 --wrap="/mnt/home1/gurdon/as2363/TOOLS/bedGraphToBigWig $file /mnt/home1/gurdon/as2363/MAMI_EggExtract/PE/LAEVIS_6.1greaterthan10k_ICeSeq.genome ${file/.bed/.bw}" 
done

#sum input coverage from different fragment-size sets - with duplicates
paste  Input_allreads_Frag150.250bpw_slid50.bed Input_allreads_Frag110.250bpw_slid50.bed Input_allreads_Frag70.250bpw_slid50.bed| awk '{print $1"\t"$2"\t"$3"\t"($4+$8+$12)}' > Input.Fragm150_110_70_sum_allreads_in_250bp_slid50.bed &&

#sum input coverage from different fragment-size sets - no duplicates
paste  Input_nodup_Frag150.250bpw_slid50.bed Input_nodup_Frag110.250bpw_slid50.bed Input_nodup_Frag70.250bpw_slid50.bed| awk '{print $1"\t"$2"\t"$3"\t"($4+$8+$12)}' > Input.Fragm150_110_70_sum_nodup_in_250bp_slid50.bed

paste Input_allreads_Frag150.250bpw_slid50.bed Input_allreads_Frag110.250bpw_slid50.bed Input_allreads_Frag70.250bpw_slid50.bed |awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$8"\t"$12"\t"($4+$8+$112)}' > Input.Fragm150_110_70_sum_alllreads_in_250bp_slid50.full.bedgraph &&

paste Input_nodup_Frag150.250bpw_slid50.bed Input_nodup_Frag110.250bpw_slid50.bed Input_nodup_Frag70.250bpw_slid50.bed |awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$8"\t"$12"\t"($4+$8+$112)}' > Input.Fragm150_110_70_sum_nodup_in_250bp_slid50.full.bedgraph


## case 2 - create bigwig out the coverage of input =============================================================
## Input fixed 50bp window - not moving window
paste Mami.Index12.mergedbam.paired_reads.Fragm150.50bpw.coverage.bed Mami.Index4.mergedbam.paired_reads.Fragm150.50bpw.coverage.bed | awk '{print $1"\t"$2"\t"$3"\t"($4+$8)}' >  ./Input_coverage/Input_nodup_Frag150.50bpw.bed &&
paste Mami.Index12.mergedbam.paired_reads.Fragm110.50bpw.coverage.bed Mami.Index4.mergedbam.paired_reads.Fragm110.50bpw.coverage.bed | awk '{print $1"\t"$2"\t"$3"\t"($4+$8)}' >  ./Input_coverage/Input_nodup_Frag110.50bpw.bed &&
paste Mami.Index12.mergedbam.paired_reads.Fragm70.50bpw.coverage.bed Mami.Index4.mergedbam.paired_reads.Fragm70.50bpw.coverage.bed | awk '{print $1"\t"$2"\t"$3"\t"($4+$8)}' >  ./Input_coverage/Input_nodup_Frag70.50bpw.bed 

paste Mami.Index12.merged.allreads.paired_reads.sorted.fixed150.50bpw.coverage.bed Mami.Index4.merged.allreads.paired_reads.sorted.fixed150.50bpw.coverage.bed | awk '{print $1"\t"$2"\t"$3"\t"($4+$8)}' >  ./Input_coverage/Input_allreads_Frag150.50bpw.bed &&
paste Mami.Index12.merged.allreads.paired_reads.sorted.fixed110.50bpw.coverage.bed Mami.Index4.merged.allreads.paired_reads.sorted.fixed110.50bpw.coverage.bed | awk '{print $1"\t"$2"\t"$3"\t"($4+$8)}' >  ./Input_coverage/Input_allreads_Frag110.50bpw.bed &&
paste Mami.Index12.merged.allreads.paired_reads.sorted.fixed70.50bpw.coverage.bed Mami.Index4.merged.allreads.paired_reads.sorted.fixed70.50bpw.coverage.bed | awk '{print $1"\t"$2"\t"$3"\t"($4+$8)}' >  ./Input_coverage/Input_allreads_Frag70.50bpw.bed

paste Input_allreads_Frag150.50bpw.bed Input_allreads_Frag110.50bpw.bed Input_allreads_Frag70.50bpw.bed |awk '{sum=($4+$8+$12); if(sum>0) {print $1"\t"$2"\t"$3"\t"$4"\t"$8"\t"$12"\t"sum"\t"$4/sum"\t"$8/sum"\t"$12/sum} else {print $1"\t"$2"\t"$3"\t"$4"\t"$8"\t"$12"\t"sum"\t"0"\t"0"\t"0}}' > Input.Fragm150_110_70_sum_allreads_in_50bpw.full.bedgraph &&
paste Input_nodup_Frag150.50bpw.bed Input_nodup_Frag110.50bpw.bed Input_nodup_Frag70.50bpw.bed |awk '{sum=($4+$8+$12); if(sum>0) {print $1"\t"$2"\t"$3"\t"$4"\t"$8"\t"$12"\t"sum"\t"$4/sum"\t"$8/sum"\t"$12/sum} else {print $1"\t"$2"\t"$3"\t"$4"\t"$8"\t"$12"\t"sum"\t"0"\t"0"\t"0}}'> Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph
```

### HMM - data preparation

``` bash

# HMM analysis for INPUT only - nodup
# median number of reads per window (50bp) = 5
#select only regions with minimum5 reads in total
awk '{if($7>=5) print $0}' Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph > Input.Fragm150_110_70_sum_above5_nodup_in_50bpw.full.bedgraph

split -l 1286787 Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph tmp_split

for file in tmp_split*
do
sbatch  -e ~/Mami_Oct_2017/Proc_Data/Input_coverage/std.err -o ~/Mami_Oct_2017/Proc_Data/Input_coverage/std.log -N 1 -n 8 --wrap="Rscript prop.test_chrom_species.R ${file} ${file}_p"
done

cat tmp_split*_* > Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph.probabilities
rm tmp_split*
```

R-script that perform the proportion test and gets as input file the bed with ChIP-seq input coverages

``` r
prop.test_chrom_species.R
args <- commandArgs(trailingOnly = TRUE)
a <- read.table(args[1], stringsAsFactors = F, sep = "\t", header = F)

test1 = seq(1,length(a$V1))
test2 = seq(1,length(a$V1))
test3 = seq(1,length(a$V1))
f1 <- 0.51
f2 <- 0.14
f3 <- 0.36

for (i in 1:length(a$V1)){
  if(i%%50000 == 0){
    print(i)}
  if(a[i,7]>0){
   test1[i] <- prop.test(a[i,4],a[i,7],p=f1)$p.value
   test2[i] <- prop.test(a[i,5],a[i,7],p=f2)$p.value
   test3[i] <- prop.test(a[i,6],a[i,7],p=f3)$p.value
   } else {
   test1[i] <- 1
   test2[i] <- 1
   test3[i] <- 1
}
pval = cbind(test1, test2, test3)

write.table(cbind(a,pval),file=args[2],sep = '\t',quote = F,col.names = F,row.names = F)
```

``` bash
cat tmp_split*_* > Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph.probabilities
#The file called Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph.probabilities
# contains the following fields:
# 1. chr
# 2.star pos
# 3.end pos
# 4.# of reads from the species 150 in interval
# 5.# of reads from the species 110 in interval
# 6.# of reads from the species 70 in interval
# 7.# of tot (sum of 150, 110, 70)
# 8. fraction of reads from the species 150 in interval
# 9.fraction of reads from the species 110 in interval
# 10.fraction of reads from the species 70 in interval
# 11. pval( from prop.test) of reads from the species 150 in interval
# 12.pval( from prop.test) of reads from the species 110 in interval
# 13.pval( from prop.test) of reads from the species 70 in interval

#  from berardi
# recodePvals<- function(p){
#     if (is.na(p)){
#        o<- NA
#     } else if (p > 0.1 ){
#        o<- 0
#     } else if (p <= 0.1 & p > 0.05) {
#        o<- 1
#     } else if (p <= 0.05 & p > 0.001) {
#        o<- 2
#     } else {
#        o<- 3
#     }
#        return(o)
#     }



awk '
    {
    if ($7==0) {p1=0;p2 =0;p3 =0}
    else if($7>0 && $11>0.1) {p1=0;}
    else if($7>0 && $11<=0.1 && $11>0.05) {p1=1;} 
    else if($7>0 && $11<=0.005 && $11>0.001) {p1=2;}
    else if($7>0 && $11<=0.001){p1=3;}
    else if($7>0 && $12>0.1) {p2=0;}
    else if($7>0 && $12<=0.1 && $12>0.05) {p2=1;} 
    else if($7>0 && $12<=0.005 && $12>0.001) {p2=2;}
    else if($7>0 && $12<=0.001){p2=3;}
    else if($7>0 && $13>0.1) {p3=0;}
    else if($7>0 && $13<=0.1 && $13>0.05) {p3=1;} 
    else if($7>0 && $13<=0.005 && $13>0.001) {p3=2;}
    else if($7>0 && $13<=0.001){p3=3;}
     print $0"\t"p1"\t"p2"\t"p3}
' Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph.probabilities >  Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph.probabilities.discrete
cut -f 14 Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph.probabilities.discrete | head -n 1000 > test_discrete_state150
```

R script that perform the HMM estimation

``` bash
cat HMM_estimate_particle.R
```

``` r
args <- commandArgs(trailingOnly = TRUE)
library("RHmm")
a <- read.table(args[1], stringsAsFactors = F, sep = "\t", header = F)
test_obs <- a$V1
test_obs[is.na(test_obs)] <- 0
hmmfit<- HMMFit(test_obs, nStates= 2, dis= 'DISCRETE')
vit<- viterbi(hmmfit, test_obs)
write.table(vit$states,file=args[2],sep = '\t',quote = F,col.names = F,row.names = F)
```

``` bash

file='test_discrete_state150'
sbatch  -p IACT -e ~/Mami_Oct_2017/Proc_Data/Input_coverage/std.err -o ~/Mami_Oct_2017/Proc_Data/Input_coverage/std.log -N 1 -n 8 --wrap="/mnt/home1/gurdon/as2363/TOOLS/R-2.15.3/bin/Rscript HMM_estimate_particle.R ${file} ${file}_hmm_s"
file='obs_150_splitaa'
sbatch  -p IACT -e ~/Mami_Oct_2017/Proc_Data/Input_coverage/std.err -o ~/Mami_Oct_2017/Proc_Data/Input_coverage/std.log -N 1 -n 8 --wrap="/mnt/home1/gurdon/as2363/TOOLS/R-2.15.3/bin/Rscript HMM_estimate_particle.R ${file} ${file}_hmm_s"

###########
150
###########
cut -f 14 Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph.probabilities.discrete|  split -l 40000  - obs_150_split
file='obs_150_splitaa'
for file in obs_150_split*
do
sbatch  -p IACT -e ~/Mami_Oct_2017/Proc_Data/Input_coverage/std.err -o ~/Mami_Oct_2017/Proc_Data/Input_coverage/std.log -N 1 -n 8 --wrap="/mnt/home1/gurdon/as2363/TOOLS/R-2.15.3/bin/Rscript HMM_estimate_particle.R ${file} ${file}_hmm_s"
done
cat *_hmm_s > Input.Fragm150_hmm_state_50bpw.full
cut -f 1,2,3 Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph.probabilities.discrete
paste <(cut -f 1,2,3 Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph.probabilities.discrete) <(cut -f 1 Input.Fragm150_hmm_state_50bpw.full)  > Input.Fragm150_hmm_state_50bpw.full.bed
rm obs_150_split*
###########
110
###########
cut -f 15 Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph.probabilities.discrete|  split -l 40000  - obs_110_split
for file in obs_110_split*
do
sbatch  -p IACT -e ~/Mami_Oct_2017/Proc_Data/Input_coverage/std.err -o ~/Mami_Oct_2017/Proc_Data/Input_coverage/std.log -N 1 -n 8 --wrap="/mnt/home1/gurdon/as2363/TOOLS/R-2.15.3/bin/Rscript HMM_estimate_particle.R ${file} ${file}_hmm_s"
done
cat obs_110*_hmm_s > Input.Fragm110_hmm_state_50bpw.full
paste <(cut -f 1,2,3 Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph.probabilities.discrete) <(cut -f 1 Input.Fragm110_hmm_state_50bpw.full)  > Input.Fragm110_hmm_state_50bpw.full.bed
rm obs_110_split*

###########
70
###########
cut -f 16 Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph.probabilities.discrete|  split -l 40000  - obs_70_split
for file in obs_70_split*
do
sbatch  -p IACT -e ~/Mami_Oct_2017/Proc_Data/Input_coverage/std.err -o ~/Mami_Oct_2017/Proc_Data/Input_coverage/std.log -N 1 -n 8 --wrap="/mnt/home1/gurdon/as2363/TOOLS/R-2.15.3/bin/Rscript HMM_estimate_particle.R ${file} ${file}_hmm_s"
done
cat obs_70*_hmm_s > Input.Fragm70_hmm_state_50bpw.full
paste <(cut -f 1,2,3 Input.Fragm150_110_70_sum_nodup_in_50bpw.full.bedgraph.probabilities.discrete) <(cut -f 1 Input.Fragm70_hmm_state_50bpw.full)  > Input.Fragm70_hmm_state_50bpw.full.bed
rm obs_70_split*




# select regions with enrichment -  state == 1
awk '{if($4==1) print $0}' Input.Fragm150_hmm_state_50bpw.full.bed > Input.Fragm150_hmm_state_50bpw.enriched.bed
awk '{if($4==1) print $0}' Input.Fragm110_hmm_state_50bpw.full.bed > Input.Fragm110_hmm_state_50bpw.enriched.bed
awk '{if($4==1) print $0}' Input.Fragm70_hmm_state_50bpw.full.bed > Input.Fragm70_hmm_state_50bpw.enriched.bed

mergeBed -i Input.Fragm150_hmm_state_50bpw.enriched.bed > Input.Fragm150_hmm_state_50bpw.enriched.merged.bed
mergeBed -i Input.Fragm110_hmm_state_50bpw.enriched.bed > Input.Fragm110_hmm_state_50bpw.enriched.merged.bed
mergeBed -i Input.Fragm70_hmm_state_50bpw.enriched.bed > Input.Fragm70_hmm_state_50bpw.enriched.merged.bed

awk '{if($4==0) print $0}' Input.Fragm150_hmm_state_50bpw.full.bed > Input.Fragm150_hmm_state_50bpw.NOTenriched.bed
awk '{if($4==0) print $0}' Input.Fragm110_hmm_state_50bpw.full.bed > Input.Fragm110_hmm_state_50bpw.NOTenriched.bed
awk '{if($4==0) print $0}' Input.Fragm70_hmm_state_50bpw.full.bed > Input.Fragm70_hmm_state_50bpw.NOTenriched.bed
mergeBed -i Input.Fragm150_hmm_state_50bpw.NOTenriched.bed > Input.Fragm150_hmm_state_50bpw.NOTenriched.merged.bed
mergeBed -i Input.Fragm110_hmm_state_50bpw.NOTenriched.bed > Input.Fragm150_hmm_state_50bpw.NOTenriched.merged.bed
mergeBed -i Input.Fragm70_hmm_state_50bpw.NOTenriched.bed > Input.Fragm150_hmm_state_50bpw.NOTenriched.merged.bed

intersectBed -a Input.Fragm110_hmm_state_50bpw.NOTenriched.merged.bed -b Input.Fragm150_hmm_state_50bpw.NOTenriched.merged.bed | sortBed -i - | mergeBed -i - > Input.Fragm150_110_hmm_state_50bpw.NOTenriched.merged.bed
intersectBed -a Input.Fragm150_110_hmm_state_50bpw.NOTenriched.merged.bed -b Input.Fragm70_hmm_state_50bpw.NOTenriched.merged.bed | sortBed -i - | mergeBed -i - > Input.Fragm150_110_70_hmm_state_50bpw.NOTenriched.merged.bed

#check size (in terms of number of bases) for each case
awk '{sum+= ($3-$2)} END {print sum}' Input.Fragm150_hmm_state_50bpw.enriched.bed
319963640
awk '{sum+= ($3-$2)} END {print sum}' Input.Fragm110_hmm_state_50bpw.enriched.bed
95122713
awk '{sum+= ($3-$2)} END {print sum}' Input.Fragm70_hmm_state_50bpw.enriched.bed
551872360

wc -l *0_hmm_state_50bpw.enriched.merged.bed

#  684354 Input.Fragm110_hmm_state_50bpw.enriched.merged.bed
# 1366899 Input.Fragm150_hmm_state_50bpw.enriched.merged.bed
#  329209 Input.Fragm70_hmm_state_50bpw.enriched.merged.bed


for file in *state_50bpw.enriched.bed
    do
    sbatch -p IACT -e err_covert.err -o temp_convert.out -N 1 -n 8 --wrap="/mnt/home1/gurdon/as2363/TOOLS/bedGraphToBigWig ${file} /mnt/home1/gurdon/as2363/MAMI_EggExtract/PE/LAEVIS_6.1greaterthan10k_ICeSeq.genome ${file/.bed/.bw}" 
done

for file in Input_nodup_Frag*0.50bpw.bed
    do
    sbatch -p IACT -e err_covert.err -o temp_convert.out -N 1 -n 8 --wrap="/mnt/home1/gurdon/as2363/TOOLS/bedGraphToBigWig ${file} /mnt/home1/gurdon/as2363/MAMI_EggExtract/PE/LAEVIS_6.1greaterthan10k_ICeSeq.genome ${file/.bed/.bw}" 
done
```
