# load HMD values for H3K4me3 and H3K27me3
setwd('/Users/angelas/Documents/MAMI_Egg_extract_analysis/SpikeIn/New_Seq/MACS2_HMD')
K4 <- read.table('HMD_macs2_nodup_190_H3K4me3_common_rep1_rep2.narrowPeak.coverage.bed',stringsAsFactors = F)
K27 <- read.table('HMD_macs2_nodup_190_H3K27me3_common_rep1_rep2.broadPeak.coverage.bed',stringsAsFactors = F)

############################################################################################################
# Functions
############################################################################################################
enrichment_in_set_interest <- function(genes_list) { 
Misregulated_transcripts_correctedIDs <- read.table("/Users/angelas/Documents/MAMI_Egg_extract_analysis/Misregulated_transcripts_correctedIDs.txt", quote="\"",stringsAsFactors = FALSE)
HouseKeeping_genes <- read.table('/Users/angelas/Documents/Laevis_6.1/Xlaevis.genes_strand_bed6.TSS_Housekeeping_genes.bed', sep = '\t', stringsAsFactors = F)
HouseKeeping_genes = HouseKeeping_genes$V4
Developmental_genes <- read.table('/Users/angelas/Documents/Laevis_6.1/Xlaevis.genes_strand_bed6.TSS_Developmental_genes.bed', sep = '\t', stringsAsFactors = F)
Developmental_genes <- Developmental_genes$V4
Misregulated_long_genes <- read.table('/Users/angelas/Documents/MAMI_Egg_extract_analysis/DE_genes_fdr5percent_corrected_names.txt', sep = '\t', stringsAsFactors = F)
Misregulated_long_genes = Misregulated_long_genes$V1
load('/Users/angelas/Documents/RNAseq_K6B_10k/HTSeq_count/K6_sperm_up.RData') #K6_sperm_up

KDM6_sperm_up <-K6_sperm_up
rm(K6_sperm_up)
load('/Users/angelas/Documents/MAMI_Egg_extract_analysis/KDM5_sperm_down.RData') #KDM5_sperm_down

load('/Users/angelas/Documents/MAMI_Egg_extract_analysis/K5_sperm_down_minus_K5_spermatid_down.RData') #kdm5 sperm down minus spermatid down
load('/Users/angelas/Documents/MAMI_Egg_extract_analysis/K6_sperm_up_minus_K6_spermatid_up.RData') #kdm6 sperm up minus spermatid up

load('/Users/angelas/Documents/MAMI_Egg_extract_analysis/External_transcriptomic_data/ZGAgenes.R') # it contains ZGA_genes
Developmental_only = setdiff(Developmental_genes,ZGA_genes)
ZGA_genes_only = setdiff(ZGA_genes,Developmental_genes)
common_developmental_ZGA = intersect(ZGA_genes,Developmental_genes)


N_misregulated_in_genes_list = length(intersect(Misregulated_transcripts_correctedIDs$V1,genes_list))
N_housekeeping_in_genes_list = length(intersect(HouseKeeping_genes,genes_list))
N_developmental_in_genes_list = length(intersect(Developmental_genes,genes_list))
N_misregulated_long_in_genes_list = length(intersect(Misregulated_long_genes,genes_list))
N_kdm5_sperm_down_in_genes_list = length(intersect(KDM5_sperm_down,genes_list))
N_kdm6_sperm_up_in_genes_list = length(intersect(KDM6_sperm_up,genes_list))
N_K5_sperm_spec_down_in_gene = length(intersect(K5_sperm_down_minus_K5_spermatid_down, genes_list))
N_K6_sperm_spec_up_in_gene = length(intersect(K6_sperm_up_minus_K6_spermatid_up, genes_list))
N_ZGA_in_genes_list = length(intersect(ZGA_genes,genes_list))
N_Developmental_only = length(intersect(Developmental_only,genes_list))
N_ZGA_genes_only = length(intersect(ZGA_genes_only,genes_list))
N_common_developmental_ZGA = length(intersect(common_developmental_ZGA,genes_list))

pt_genes_list_mis=prop.test(c(N_misregulated_in_genes_list, length(Misregulated_transcripts_correctedIDs$V1)),c(length(genes_list),34373),alternative = "greater")$p.value
pt_genes_list_house = prop.test(c(N_housekeeping_in_genes_list,length(HouseKeeping_genes)),c(length(genes_list),34373),alternative = "greater")$p.value
pt_genes_list_dev = prop.test(c(N_developmental_in_genes_list,length(Developmental_genes)),c(length(genes_list),34373),alternative = "greater")$p.value
pt_genes_list_mis_long = prop.test(c(N_misregulated_long_in_genes_list,length(Misregulated_long_genes)),c(length(genes_list),34373),alternative = "greater")$p.value
pt_genes_list_kdm5_sperm_down = prop.test(c(N_kdm5_sperm_down_in_genes_list,length(KDM5_sperm_down)),c(length(genes_list),34373),alternative = "greater")$p.value
pt_genes_list_kdm6_sperm_up = prop.test(c(N_kdm6_sperm_up_in_genes_list,length(KDM6_sperm_up)),c(length(genes_list),34373),alternative = "greater")$p.value

pt_genes_list_K5_sperm_spec_down = prop.test(c(N_K5_sperm_spec_down_in_gene,length(K5_sperm_down_minus_K5_spermatid_down)),c(length(genes_list),34373),alternative = "greater")$p.value
pt_genes_list_K6_sperm_spec_up = prop.test(c(N_K6_sperm_spec_up_in_gene,length(K6_sperm_up_minus_K6_spermatid_up)),c(length(genes_list),34373),alternative = "greater")$p.value

pt_genes_list_ZGA = prop.test(c(N_ZGA_in_genes_list,length(ZGA_genes)),c(length(genes_list),34373),alternative = "greater")$p.value
pt_genes_list_Developmental_only = prop.test(c(N_Developmental_only,length(Developmental_only)),c(length(genes_list),34373),alternative = "greater")$p.value
pt_genes_list_ZGA_only = prop.test(c(N_ZGA_genes_only,length(ZGA_genes_only)),c(length(genes_list),34373),alternative = "greater")$p.value
pt_genes_list_common_developmental_ZGA = prop.test(c(N_common_developmental_ZGA,length(common_developmental_ZGA)),c(length(genes_list),34373),alternative = "greater")$p.value

pt_genes_list = c(pt_genes_list_mis,pt_genes_list_house,pt_genes_list_dev,
                  pt_genes_list_mis_long,pt_genes_list_kdm5_sperm_down,
                  pt_genes_list_K5_sperm_spec_down,pt_genes_list_K6_sperm_spec_up,
                  pt_genes_list_kdm6_sperm_up,pt_genes_list_ZGA,
                  pt_genes_list_Developmental_only,pt_genes_list_ZGA_only,pt_genes_list_common_developmental_ZGA)
names(pt_genes_list)=c('Mis','House','Dev','Mis_long','KDM5','KDM6',
                       'KDM5sperm_down_nospermatid','KDM6sperm_up_nospermatid','ZGA',
                       'Developmental_only','ZGA_only','common_developmental_ZGA')
print(round(pt_genes_list,3))
return(pt_genes_list)
}

GOenrichment_topgo <- function(genes_list){
# ================================================================================================================================================================
# GO ENRICHMENT 150 pp
library(topGO)
geneID2GO <- readMappings(file = "/Users/angelas/Documents/MAMI_Egg_extract_analysis/MappingGO_Laevis.map")
geneNames <- names(geneID2GO)

geneList_input <- factor(as.integer(geneNames%in%genes_list))
names(geneList_input) <- geneNames
GOdata_BP <- new("topGOdata", ontology = "BP", allGenes = geneList_input, annot = annFUN.gene2GO, gene2GO = geneID2GO)
# test enrichment with different algorithms BP
resultFisher_BP <- runTest(GOdata_BP, algorithm = "classic", statistic = "fisher")
resultKS_BP <- runTest(GOdata_BP, algorithm = "classic", statistic = "ks")
resultKS_BP.elim <- runTest(GOdata_BP, algorithm = "elim", statistic = "ks")

# analysis of the results BP
#allRes_BP <- GenTable(GOdata_BP, classicFisher = resultFisher_BP,topNodes = 50)
allRes_BP <- GenTable(GOdata_BP, classicFisher = resultFisher_BP, classicKS = resultKS_BP, elimKS = resultKS_BP.elim, orderBy = "elimKS", ranksOf = "classicFisher", topNodes = length(resultKS_BP@score))
#print(allRes_BP[which(as.numeric(allRes_BP$classicFisher)<=0.05),c(1,2,6)])
annotations_cluster_filtered = allRes_BP[which(as.numeric(allRes_BP$classicFisher)<=0.1 & as.numeric(allRes_BP$classicKS)<=0.1 & as.numeric(allRes_BP$elimKS)<=0.1),]
annotations_cluster_filtered_sorted = annotations_cluster_filtered[order(as.numeric(annotations_cluster_filtered$classicKS), decreasing = T),]
annotations_cluster_filtered_sorted = annotations_cluster_filtered_sorted[which(annotations_cluster_filtered_sorted$Annotated<500),]
#annotation_cluster_file_name = '150pp1_genes_topGO_BP.txt'
#annotation_filtered_cluster_file_name = '150pp1_genes_topGO_BP_filtered.txt'
#write.table(allRes_BP,file = annotation_cluster_file_name, sep = "\t", quote = F,row.names =F)
#write.table(annotations_cluster_filtered,file =annotation_filtered_cluster_file_name, sep = "\t", quote = F,row.names =F)
GOres <- list('allRes_BP' = allRes_BP,'filteredRed' = annotations_cluster_filtered_sorted)
return(GOres)
}
Genomic.locations.foldChange <- function(initalBedFile,Nrand) {
# step1 - randomize initial data
  A = matrix(nrow=8,ncol=1)
  B = matrix(nrow=8,ncol=Nrand)
  names(A) = c('introns','exons','intergenic','TSS','gene_body','gene_body+prom','gene_body-prom','P300')
  rownames(B) = c('introns','exons','intergenic','TSS','gene_body','gene_body+prom','gene_body-prom','P300')
  colnames(B) = paste('rand_',seq(1:Nrand),sep = "")
  random_files = paste(gsub('.bed',replacement='.random.',initalBedFile),seq(1,Nrand),'.bed',sep = "")
  
  
  for (i in 1:Nrand){
  system(paste("bedtools shuffle -i ",initalBedFile ," -g /Users/angelas/Documents/MAMI_Egg_extract_analysis/LAEVIS_6.1greaterthan10k.genome -seed ", i," | bedtools sort -i -  >", random_files[i],sep=""))
  }
  # step2 - count intersection actual data
  A[1] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/Xlaevis.introns.bed -a ",initalBedFile," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
  
  A[2] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/Xlaevis.exons.sorted.bed -a ",initalBedFile," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
  
  A[3] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/Xlaevis.intergenic.sorted.merged.bed -a ",initalBedFile," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
  
  A[4] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/TSS_xlaevis_allgenes.slop1000.bed -a ",initalBedFile," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
  
  A[5] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/Xlaevis.genes_strand_bed6_sorted.bed -a ",initalBedFile ," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
  
  A[6] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/Xlaevis.genes_plus_promoter_strand_bed6_sorted.bed -a ",initalBedFile ," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
  
  A[7] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/Xlaevis.intergenic_minus_promoter_strand_bed6_sorted.bed -a ",initalBedFile ," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
  
  A[8] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/p300_stage10.5_Xla6.1.sorted.bed -a ",initalBedFile ," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
  
  # step3 - count intersection random data
  for (i in seq(1,Nrand)) {
  
    B[1,i] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/Xlaevis.introns.bed -a ",random_files[i]," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
    
    B[2,i] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/Xlaevis.exons.sorted.bed -a ",random_files[i]," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
    
    B[3,i] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/Xlaevis.intergenic.sorted.merged.bed -a ",random_files[i]," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
    
    B[4,i] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/TSS_xlaevis_allgenes.slop1000.bed -a ",random_files[i]," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
    
    B[5,i] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/Xlaevis.genes_strand_bed6_sorted.bed -a ",random_files[i] ," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
    
    B[6,i] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/Xlaevis.genes_plus_promoter_strand_bed6_sorted.bed -a ",random_files[i] ," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
    
    B[7,i] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/Xlaevis.intergenic_minus_promoter_strand_bed6_sorted.bed -a ",random_files[i] ," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
    
    B[8,i] = as.numeric(system(paste("intersectBed -b /Users/angelas/Documents/MAMI_Egg_extract_analysis/p300_stage10.5_Xla6.1.sorted.bed -a ",random_files[i] ," -wa | sortBed -i -| uniq | wc -l",sep = ""),intern = TRUE))
      
  }
  counts_intersections <- list("actual" = A, "random" = B)

return(counts_intersections)

}
