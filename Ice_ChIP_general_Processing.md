Xenopus all code for general processing
================
Angela Simeone


General outline
---------------

Processing of ICe-ChIP-seq data is similar to the processing of standard ChIP. It requires similar preprocessing (cut ataptors/low quality reads, aligment, peak calling, etc.).

Differences arises in the reference genome to use. Becuase of the calibration step that relies on the signal at specific sequencese (ladder), it is necessary to consider them during the alignment. As proposed in the \[original paper\] (<https://www.cell.com/molecular-cell/fulltext/S1097-2765(15)00304-4>), the sequences of the nucleosomes reconstituted from recombinant and semisynthetic histones are known and can be pasted to the reference genome before alignent or could be used to run a specific alignment to them.

For our study we included the ladder sequences in the genome reference fasta and we later quantified the signal.

I will list the tools and commands used to perform processing. Those have been used on a SLURM system and, before

### Cut adapters - CUTADAPT

``` bash

# paired-end sequencing

cutadapt -a AGATCGGAAGAGC -A AGATCGGAAGAG -o $out.R1.trimmed.fq.gz -p $out.R2.trimmed.fq.gz $input.p1.fq.gz $input.p2.fq.gz


# in our case we built a tab-separated table with the pairs (reads1, reads2, label for the output file)
qdir2='~/Mami_Oct_2017/Mami_HiSeq1500_171115'
outdir='~/Mami_Oct_2017/BWA_MEM_out'
IFS=$'\t'
while read p1 p2 p3; do
    echo "================="
    echo sbatch -p IACT -N 1 -n 6 -o $fqdir/tmp_new.log -o $fqdir/tmp_new.err --wrap="cutadapt -a AGATCGGAAGAGC -A AGATCGGAAGAG -o $outdir/$p3.R1.trimmed.fq.gz -p $outdir/$p3.R2.trimmed.fq.gz $fqdir2/$p1 $fqdir2/$p2"
done < Experiment_pairs.txt

cat ./HiSeq1500_171024_AHWFKHBCXY/Experiment_pairs_new.txt
# MO_ICe1_S1_R1_001.fastq.gz    MO_ICe1_S1_R2_001.fastq.gz  MO_ICe1_S1
# MO_ICe2_S2_R1_001.fastq.gz    MO_ICe2_S2_R2_001.fastq.gz  MO_ICe1_S2
# MO_ICe3_S3_R1_001.fastq.gz    MO_ICe3_S3_R2_001.fastq.gz  MO_ICe1_S3
# MO_ICe4_S4_R1_001.fastq.gz    MO_ICe4_S4_R2_001.fastq.gz  MO_ICe1_S4
# MO_ICe5_S5_R1_001.fastq.gz    MO_ICe5_S5_R2_001.fastq.gz  MO_ICe1_S5
# MO_ICe6_S6_R1_001.fastq.gz    MO_ICe6_S6_R2_001.fastq.gz  MO_ICe1_S6
```

### Alignment - BWA\_MEM

``` bash
# need to define
# - genome_file
# - output file name (sam)
bwa mem -M -t 12 $genomefile $fqdir/$p1 $fqdir/$p2 > $fqdir/$p3.sam


# for XenopusL.+ICEsequences
genomefile='~/MAMI_EggExtract/PE/LAEVIS_6.1greaterthan10k_ICeSeq.fa'
fqdir='~/Mami_Oct_2017/BWA_MEM_out'
IFS=$'\t'
while read p1 p2 p3; do
echo "================="

echo sbatch -p IACT -N 1 -n 6 -o $fqdir/tmp_new.log -o $fqdir/tmp_new.err --wrap="bwa mem -M -t 12 $genomefile $fqdir/$p1 $fqdir/$p2 > $fqdir/$p3.sam"
done < Experimental_pairs.txt
```

### Processing of aligment (.sam file)

For the samtools flag explanation, please refer to [Decoding SAM flags](https://broadinstitute.github.io/picard/explain-flags.html)

``` bash

# convert sam --> bam
samtools view -hbS $aln_dir/$samfile > $aln_dir/${samfile/.sam/.bam}

# sort bam
samtools sort -@ 6 $bamfile ${bamfile/.bam/.sorted}

#extract only properly-paired reads and then sort
samtools view -f2 -b $aln_dir/$bamfile > $aln_dir/${bamfile/.sorted.bam/.paired_reads.bam} && samtools sort -@ 6 $aln_dir/${bamfile/.sorted.bam/.paired_reads.bam} $aln_dir/${bamfile/.sorted.bam/.paired_reads.sorted}

#merge sequenced and resequenced data (e.g. in the case of the SAME SAMPLE sequenced multiple times for achiving higher sequecing depth)
samtools merge out.mergedbam.paired_reads.bam sequecing_round1.paired_reads.sorted.bam sequecing_round2.paired_reads.sorted.bam

#sort merged bam before marking duplicates
samtools sort -@ 6 $bam ${bam/.mergedbam.paired_reads.bam/.mergedbam.paired_reads.sorted}

# mark duplicates with Picard && index result bam
java -jar ~/TOOLS/Picard_2.14.0_folder/picard.jar MarkDuplicates I=$aln_dir/${bampe} O=$aln_dir/${bampe/.sorted.bam/.sorted.dedup.bam} M=$aln_dir/${bampe/.sorted.bam/.sorted.dedup.txt}&& 
samtools index $aln_dir/${bampe/.sorted.bam/.sorted.dedup.bam}

## === alternative case: create bam with mappin quality 20 and filter or not duplicates (https://broadinstitute.github.io/picard/explain-flags.html)

# filter out duplicates
samtools view -hb -F 1024 -q 20 $aln_dir/$bamdedup >$out_dir/${bamdedup/.sorted.dedup.bam/.sorted.rm_dup_q20.bam} && 
samtools sort $out_dir/${bamdedup/.sorted.dedup.bam/.sorted.rm_dup_q20.bam} $out_dir/${bamdedup/.sorted.dedup.bam/.sorted.rm_dup_q20.sorted}

# keep duplicates
samtools view -hb -q 20 $aln_dir/$bamdedup >$out_dir/${bamdedup/.sorted.dedup.bam/.sorted.dedup_q20.bam} && 
samtools sort $out_dir/${bamdedup/.sorted.dedup.bam/.sorted.dedup_q20.bam} $out_dir/${bamdedup/.sorted.dedup.bam/.sorted.dedup_q20.sorted}


# stats - to be run on the various bam - initial bam, merged bam, duplicates-depleted bam
samtools flagstat ${bamfile} > $workdir/temp2.txt

# bedgraph and bw track files generation for browsing data on genome-browser
bedtools genomecov -ibam $bamfile -g /mnt/home1/gurdon/as2363/MAMI_EggExtract/PE/LAEVIS_6.1greaterthan10k_ICeSeq.genome -bg  > ${bamfile/.bam/.bedgraph} && /mnt/home1/gurdon/as2363/TOOLS/bedGraphToBigWig ${bamfile/.bam/.bedgraph} /mnt/home1/gurdon/as2363/MAMI_EggExtract/PE/LAEVIS_6.1greaterthan10k_ICeSeq.genome ${bamfile/.bam/.bw}


#generate BAMPE files (the two paired reads are reported on the same file): this require to sort by read name
samtools sort -n $bamdir/$bamfile $bamdir/${bamfile/.rm_dup_q20.sorted.bam/.rm_dup_q20.sortedByName} &&  samtools fixmate $bamdir/${bamfile/.rm_dup_q20.sorted.bam/.rm_dup_q20.sortedByName.fixed.bam} $bamdir/${bamfile/.rm_dup_q20.sorted.bam/.rm_dup_q20.sortedByName.fixed.fixed.bam} && bedtools bamtobed -bedpe -i $bamdir/${bamfile/.rm_dup_q20.sorted.bam/.rm_dup_q20.sortedByName.fixed.fixed.bam} > $bamdir/${bamfile/.rm_dup_q20.sorted.bam/.rm_dup_q20.sortedByName.fixed.fixed.bed}


#check number of total number of pairs
wc -l $bed_rm_dup_q20.sortedByName.fixed.fixed.bed
```

### Fom BEDPE files select the various fragments

This needs to be run on indvidual replicates and on merged ones.

``` bash
# for each replicate and also for merged sampels generate the different fragments
for file in *.bed
    do
    echo "========"
    echo $file
    
    # selection of the 3 different fragment length - ranges are defined base on previous published reports
    awk '{ if($5<=80 && $5>60) print $0"\t"70; else if($5>115 && $5<=190) print $0"\t"150; else if($5>100 && $5<=115) print $0"\t"110; else print $0"\t"0}' $file > ${file/.bed/_flag.bed}

    # selection of just 190 -regardless minimum length
    awk '{if($5<=190){print $0"\t"190}}' $file > ${file/.bed/_Fragm190.bed}
done

#split the flagged BEDPE file in 3 indipendend bed files
for file in *_flag.bed
    do
    echo "========"
    echo $file
    nohup grep 70 $file | sort -k 1,1 -k2,2n - > ${file/_flag.bed/_Fragm70.sorted.bed}
    nohup grep 110 $file | sort  -k 1,1 -k2,2n - > ${file/_flag.bed/_Fragm110.sorted.bed}
    nohup grep 150 $file | sort  -k 1,1 -k2,2n - > ${file/_flag.bed/_Fragm150.sorted.bed}
done
```

### Compute coverage over ladder

``` bash

for file in *.merged.allreads.paired_reads.sorted.fixed190.bed
do
sbatch -p IACT -e $aln_dir/tmp_cov.err -o $aln_dir/tmp_cov.out -n1 -n8 --wrap="bedtools coverage -a Ladder_bed.bed -b $file -counts > ${file/.bed/.ladder.coverage.bed}"
done
```

### Compute coverage over the genome binned in windows 50bp-wide

``` bash
windowfile='/mnt/home1/gurdon/as2363/MAMI_EggExtract/PE/LAEVIS_6.1greaterthan10k_ICeSeq_50bpwindows.sorted.bed'


for file in *.merged.allreads.paired_reads.sorted.fixed*0.bed
    do
    sbatch -p IACT -e tmp_covall.err -o tmp_covall.out -N 1 -n8 --wrap="bedtools coverage -a $windowfile -b $file -counts > ${file/.bed/.50bpw.coverage.bed} && /mnt/home1/gurdon/as2363/TOOLS/bedGraphToBigWig ${file/.bed/.50bpw.coverage.bed} $genomefile ${file/.bed/.50bpw.coverage.bw}"
done

for file in *.mergedbam.paired_reads*sorted.bed
    do
    sbatch -p IACT -e tmp_covall2.err -o tmp_covall2.out -N 1 -n8 --wrap="bedtools coverage -a $windowfile -b $file -counts > ${file/.sorted.bed/.50bpw.coverage.bed} && /mnt/home1/gurdon/as2363/TOOLS/bedGraphToBigWig ${file/.sorted.bed/.50bpw.coverage.bed} $genomefile ${file/.sorted.bed/.50bpw.coverage.bw} "
done}
```

### MACS2 peak calling

BEDPE files have been converted into bam to run MACs2 peak calling. MACS2 can also handle BED files so various approaches are possible.

``` bash
# BEDPE --> BAMPE
bedtools bedtobam -i $file -g /mnt/home1/gurdon/as2363/MAMI_EggExtract/PE/LAEVIS_6.1greaterthan10k_ICeSeq.genome | samtools sort -  ${file/.paired_reads.sorted.fixed190.bed/.sorted}

# call peaks on BED file - narrow
macs2 callpeak -t ${narrow_hist_mod_IP}.mergedbam.paired_reads.Fragm190.sorted.bed -c ${narrow_hist_mod_input}.mergedbam.paired_reads.Fragm190.sorted.bed -f BEDPE --keep-dup 'all' -n macs2_nodup_190_narrow_hist_mod --gsize=2.6e9 -B -q 0.01 --verbose 2

# call peaks on BED file - broad
macs2 callpeak -t ${narrow_hist_mod_IP}.mergedbam.paired_reads.Fragm190.sorted.bed -c ${narrow_hist_mod_input}.mergedbam.paired_reads.Fragm190.sorted.bed -f BEDPE --keep-dup 'all' -n macs2_nodup_190_narrow_hist_mod --gsize=2.6e9 -B -q 0.01 --verbose 2
```

### MACS2 peak calling confirmed peaks

In the case of calling peaks in individual replicates, confimed peaks are those present in all replicates (2 in this case) or in the number of replicates desidered (i.e. 2 out of 3 etc...) In the case of calling peaks on merged data, confirmed peaks are those indentified directly after peak calling.

``` bash

# loop over alignent files using the same peak file
bedtools coverage -a ${peak_file} -b ${alignemnt_file} -counts > ${coverage_at_peaks}.bed
```

### Compute HMD giving the coverage over ladder

Coverages over ladder sequences is used according to the formula of the original paper. - `Analysis_ladder.R` -

``` r
setwd('~/Documents/MAMI_Egg_extract_analysis/SpikeIn/Ladder_coverage')
H3K4me3_rep1 <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/Ladder_coverage/Mami_HiSeq1500_171115.Index5_LadderCov.bed", header=F, quote="", stringsAsFactors = F)
H3K4me3_rep2 <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/Ladder_coverage/Mami_HiSeq1500_171115.Index19_LadderCov.bed", header=F)
H3K27me3_rep1 <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/Ladder_coverage/Mami_HiSeq1500_171115.Index6_LadderCov.bed", header=F, quote="", stringsAsFactors = F)
H3K27me3_rep2 <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/Ladder_coverage/Mami_HiSeq1500_171115.Index2_LadderCov.bed", header=F)

#H3K4me3_rep1_test <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/MACS2_nomodel_peaks/Coverage_on_Ladder/Mami_HiSeq1500_171115.Index5_no221_LadderCov.bed",header=F, quote="", stringsAsFactors = F)

Input_1 <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/Ladder_coverage/Mami_HiSeq1500_171115.Index12_LadderCov.bed", header=F)
Input_2 <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/Ladder_coverage/Mami_HiSeq1500_171115.Index4_LadderCov.bed", header=F)

Ladder_names_H3K4me3 = c('601_C094_Rev','601_C034_Rev','601_C043_Rev','601_C044_Rev','601_C070_Rev','601_C061_Rev','601_C083_Rev','601_C075_Rev')
names(Ladder_names_H3K4me3) = c('100%','80%','60%','40%','20%','10%','5%','1%')

Ladder_names_H3K27me3 = c('601_C093_Rev','601_C038_Rev','601_C005_Rev','601_C041_Rev','601_C008_Rev','601_C010_Rev','601_C017_Rev','601_C056_Rev')
names(Ladder_names_H3K27me3) = c('100%','80%','60%','40%','20%','10%','5%','1%')

Ladder_names_WT = c()

match(Ladder_names_H3K4me3,H3K4me3_rep1$V1)
H3K4me3_rep1$V4[match(Ladder_names_H3K4me3,H3K4me3_rep1$V1)]
pdf('/Users/angelas/Documents/MAMI_Egg_extract_analysis/SpikeIn/H3K4me3_ladder_rep1.pdf')
plot(c(100,80,60,40,20,10,5,1),H3K4me3_rep1$V4[match(Ladder_names_H3K4me3,H3K4me3_rep1$V1)],main = "H3K4me3_rep1")
dev.off()
match(Ladder_names_H3K4me3,H3K4me3_rep2$V1)
H3K4me3_rep2$V4[match(Ladder_names_H3K4me3,H3K4me3_rep2$V1)]
pdf('/Users/angelas/Documents/MAMI_Egg_extract_analysis/SpikeIn/H3K4me3_ladder_rep2.pdf')
plot(c(100,80,60,40,20,10,5,1),H3K4me3_rep2$V4[match(Ladder_names_H3K4me3,H3K4me3_rep2$V1)],main = "H3K4me3_rep2")
dev.off()
match(Ladder_names_H3K27me3,H3K27me3_rep1$V1)
H3K27me3_rep1$V4[match(Ladder_names_H3K27me3,H3K27me3_rep1$V1)]
pdf('/Users/angelas/Documents/MAMI_Egg_extract_analysis/SpikeIn/H3K27me3_ladder_rep1.pdf')
plot(c(100,80,60,40,20,10,5,1),H3K27me3_rep1$V4[match(Ladder_names_H3K27me3,H3K27me3_rep1$V1)],main = "H3K27me3_rep1")
dev.off()
match(Ladder_names_H3K27me3,H3K27me3_rep2$V1)
H3K27me3_rep2$V4[match(Ladder_names_H3K27me3,H3K27me3_rep2$V1)]
pdf('/Users/angelas/Documents/MAMI_Egg_extract_analysis/SpikeIn/H3K27me3_ladder_rep2.pdf')
plot(c(100,80,60,40,20,10,5,1),H3K27me3_rep2$V4[match(Ladder_names_H3K27me3,H3K27me3_rep2$V1)],main = "H3K27me3_rep2")
dev.off()

pdf('/Users/angelas/Documents/MAMI_Egg_extract_analysis/SpikeIn/Input_inH3K4me3_rep1_ladder.pdf')
plot(c(100,80,60,40,20,10,5,1),Input_1$V4[match(Ladder_names_H3K4me3,Input_1$V1)],main = "InputInH3K4me3_rep1")
dev.off()
pdf('/Users/angelas/Documents/MAMI_Egg_extract_analysis/SpikeIn/Input_inH3K4me3_rep2_ladder.pdf')
plot(c(100,80,60,40,20,10,5,1),Input_2$V4[match(Ladder_names_H3K4me3,Input_2$V1)],main = "InputInH3K4me3_rep2")
dev.off()
pdf('/Users/angelas/Documents/MAMI_Egg_extract_analysis/SpikeIn/Input_inH3K27me3_rep1_ladder.pdf')
plot(c(100,80,60,40,20,10,5,1),Input_1$V4[match(Ladder_names_H3K27me3,Input_1$V1)],main = "InputInH3K27me3_rep1")
dev.off()
pdf('/Users/angelas/Documents/MAMI_Egg_extract_analysis/SpikeIn/Input_inH3K27me3_rep2_ladder.pdf')
plot(c(100,80,60,40,20,10,5,1),Input_2$V4[match(Ladder_names_H3K27me3,Input_2$V1)],main = "InputInH3K27me3_rep2")
dev.off()

Input_1$V4[Input_1$V1 %in% Ladder_names_H3K4me3]
Input_2$V4[Input_1$V1 %in% Ladder_names_H3K4me3]
Input_1$V4[Input_1$V1 %in% Ladder_names_H3K27me3]
Input_2$V4[Input_2$V1 %in% Ladder_names_H3K27me3]

average_ladder_H3K4me3_rep1 = mean(H3K4me3_rep1$V4[H3K4me3_rep1$V1 %in% Ladder_names_H3K4me3])
average_ladder_H3K4me3_rep2 = mean(H3K4me3_rep2$V4[H3K4me3_rep2$V1 %in% Ladder_names_H3K4me3])
average_ladder_H3K4me3_Input_1 = mean(Input_1$V4[Input_1$V1 %in% Ladder_names_H3K4me3])
average_ladder_H3K4me3_Input_2 = mean(Input_2$V4[Input_2$V1 %in% Ladder_names_H3K4me3])

average_ladder_H3K27me3_rep1 = mean(H3K27me3_rep1$V4[H3K27me3_rep1$V1 %in% Ladder_names_H3K27me3])
average_ladder_H3K27me3_rep2 = mean(H3K27me3_rep2$V4[H3K27me3_rep2$V1 %in% Ladder_names_H3K27me3])
average_ladder_H3K27me3_Input_1 = mean(Input_1$V4[Input_1$V1 %in% Ladder_names_H3K27me3])
average_ladder_H3K27me3_Input_2 = mean(Input_2$V4[Input_2$V1 %in% Ladder_names_H3K27me3])


IP_efficiency_H3K4me3_rep1 = average_ladder_H3K4me3_rep1/average_ladder_H3K4me3_Input_1
IP_efficiency_H3K4me3_rep2 = average_ladder_H3K4me3_rep2/average_ladder_H3K4me3_Input_2

IP_efficiency_H3K27me3_rep1 = average_ladder_H3K27me3_rep1/average_ladder_H3K27me3_Input_1
IP_efficiency_H3K27me3_rep2 = average_ladder_H3K27me3_rep2/average_ladder_H3K27me3_Input_2


H3K4me3_peaks1 <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/MACS2_nomodel_peaks/Coverage_on_peaks/Mami_HiSeq1500_171115.Index5_H3K4me3Common_peaks_coverage.bed", header=F, stringsAsFactors = F)
H3K4me3_peaks1_input1 <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/MACS2_nomodel_peaks/Coverage_on_peaks/Mami_HiSeq1500_171115.Index12_H3K4me3Common_peaks_coverage.bed", header=F, stringsAsFactors = F)

H3K4me3_peaks2 <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/MACS2_nomodel_peaks/Coverage_on_peaks/Mami_HiSeq1500_171115.Index19_H3K4me3Common_peaks_coverage.bed", header=F, stringsAsFactors = F)
H3K4me3_peaks2_input2 <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/MACS2_nomodel_peaks/Coverage_on_peaks/Mami_HiSeq1500_171115.Index4_H3K4me3Common_peaks_coverage.bed", header=F, stringsAsFactors = F)


HMD_H3K4me3_rep1 = cbind(H3K4me3_peaks1[,1:3],(H3K4me3_peaks1[,6]/H3K4me3_peaks1_input1[,6])/IP_efficiency_H3K4me3_rep1*100)
HMD_H3K4me3_rep2 = cbind(H3K4me3_peaks2[,1:3],(H3K4me3_peaks2[,6]/H3K4me3_peaks2_input2[,6])/IP_efficiency_H3K4me3_rep2*100)

Corrected_H3K4me3_peaks1 = H3K4me3_peaks1[,6]/H3K4me3_peaks1_input1[,6]/IP_efficiency_H3K4me3_rep1*100
Corrected_H3K4me3_peaks2 = H3K4me3_peaks2[,6]/H3K4me3_peaks2_input2[,6]/IP_efficiency_H3K4me3_rep2*100


write.table(HMD_H3K4me3_rep1,'/Users/angelas/Documents/MAMI_Egg_extract_analysis/SpikeIn/HMD_H3K4me3_rep1.bedgraph',col.names = F,row.names = F, quote = F,sep ='\t')
write.table(HMD_H3K4me3_rep2,'/Users/angelas/Documents/MAMI_Egg_extract_analysis/SpikeIn/HMD_H3K4me3_rep2.bedgraph',col.names = F,row.names = F, quote = F,sep ='\t')
hist(Corrected_H3K4me3_peaks1[,6])
hist(Corrected_H3K4me3_peaks2[,6])


H3K27me3_peaks1 <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/MACS2_nomodel_peaks/Coverage_on_peaks/Mami_HiSeq1500_171115.Index6_H3K27me3Common_peaks_coverage.bed", header=F, stringsAsFactors = F)
H3K27me3_peaks1_input1 <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/MACS2_nomodel_peaks/Coverage_on_peaks/Mami_HiSeq1500_171115.Index12_H3K27me3Common_peaks_coverage.bed", header=F, stringsAsFactors = F)

H3K27me3_peaks2 <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/MACS2_nomodel_peaks/Coverage_on_peaks/Mami_HiSeq1500_171115.Index2_H3K27me3Common_peaks_coverage.bed", header=F, stringsAsFactors = F)
H3K27me3_peaks2_input2 <- read.delim("~/Documents/MAMI_Egg_extract_analysis/SpikeIn/MACS2_nomodel_peaks/Coverage_on_peaks/Mami_HiSeq1500_171115.Index4_H3K27me3Common_peaks_coverage.bed", header=F, stringsAsFactors = F)

Corrected_H3K27me3_peaks1 = H3K27me3_peaks1[,6]/H3K27me3_peaks1_input1[,6]/IP_efficiency_H3K27me3_rep1*100
Corrected_H3K27me3_peaks2 = H3K27me3_peaks2[,6]/H3K27me3_peaks2_input2[,6]/IP_efficiency_H3K27me3_rep2*100

HMD_H3K27me3_rep1 = cbind(H3K27me3_peaks1[,1:3],(H3K27me3_peaks1[,6]/H3K27me3_peaks1_input1[,6]/IP_efficiency_H3K27me3_rep1*100))
HMD_H3K27me3_rep2 = cbind(H3K27me3_peaks2[,1:3],(H3K27me3_peaks2[,6]/H3K27me3_peaks2_input2[,6]/IP_efficiency_H3K27me3_rep2*100))
write.table(HMD_H3K27me3_rep1,'/Users/angelas/Documents/MAMI_Egg_extract_analysis/SpikeIn/HMD_H3K27me3_rep1.bedgraph',col.names = F,row.names = F, quote = F,sep ='\t')
write.table(HMD_H3K27me3_rep2,'/Users/angelas/Documents/MAMI_Egg_extract_analysis/SpikeIn/HMD_H3K27me3_rep2.bedgraph',col.names = F,row.names = F, quote = F,sep ='\t')
```

### Calibrate signal at peaks using HMD factor

``` bash
paste Mami.Index5.mergedbam.paired_reads.Fragm190.sorted.nodup_190_H3K4me3_common.bed Mami.Index19.mergedbam.paired_reads.Fragm190.sorted.nodup_190_H3K4me3_common.bed Mami.Index12.mergedbam.paired_reads.Fragm190.sorted.nodup_190_H3K4me3_common.bed Mami.Index4.mergedbam.paired_reads.Fragm190.sorted.nodup_190_H3K4me3_common.bed | awk '{sum_IP = ($11+$22);sum_in=($33+$44); if((sum_in != 0 && sum_IP !=0 )|| (sum_in!= 0 && sum_IP ==0) ) {print $1"\t"$2"\t"$3"\t"(sum_IP/sum_in)*100/5.810811;}else if(sum_in==0 && sum_IP!=0) {a = -10; print $1"\t"$2"\t"$3"\t"a;} else if(sum_in==0 && sum_IP==0){a = -1; print $1"\t"$2"\t"$3"\t"a;} }' >   /mnt/home1/gurdon/as2363/Mami_Oct_2017/Proc_Data/MACS2_processedPeaks/HMD_macs2_nodup_190_H3K4me3_common_rep1_rep2.narrowPeak.coverage.bed

paste Mami.Index6.mergedbam.paired_reads.Fragm190.sorted.nodup_190_H3K27me3_broad.bed Mami.Index2.mergedbam.paired_reads.Fragm190.sorted.nodup_190_H3K27me3_broad.bed Mami.Index12.mergedbam.paired_reads.Fragm190.sorted.nodup_190_H3K27me3_broad.bed Mami.Index4.mergedbam.paired_reads.Fragm190.sorted.nodup_190_H3K27me3_broad.bed| awk '{sum_IP = ($10+$20);sum_in=($30+$40); if((sum_in != 0 && sum_IP !=0 )|| (sum_in!= 0 && sum_IP ==0) ) {print $1"\t"$2"\t"$3"\t"(sum_IP/sum_in)*100/1.893617;}else if(sum_in==0 && sum_IP!=0) {a = -10; print $1"\t"$2"\t"$3"\t"a;} else if(sum_in==0 && sum_IP==0){a = -1; print $1"\t"$2"\t"$3"\t"a;} }' >   /mnt/home1/gurdon/as2363/Mami_Oct_2017/Proc_Data/MACS2_processedPeaks/HMD_macs2_nodup_190_H3K27me3_common_rep1_rep2.broadPeak.coverage.bed

awk '{if ($4>=80) print $0}' HMD_macs2_nodup_190_H3K27me3_common_rep1_rep2.broadPeak.coverage.bed >HMD80_macs2_nodup_190_H3K27me3_common_rep1_rep2.broadPeak.coverage.bed
awk '{if ($4>=80) print $0}' HMD_macs2_nodup_190_H3K4me3_common_rep1_rep2.narrowPeak.coverage.bed > HMD80_macs2_nodup_190_H3K4me3_common_rep1_rep2.narrowPeak.coverage.bed

awk '{if ($4>=20 && $4<80) print $0}' HMD_macs2_nodup_190_H3K27me3_common_rep1_rep2.broadPeak.coverage.bed > HMD20_80_macs2_nodup_190_H3K27me3_common_rep1_rep2.broadPeak.coverage.bed
awk '{if ($4>=20 && $4<80) print $0}' HMD_macs2_nodup_190_H3K4me3_common_rep1_rep2.narrowPeak.coverage.bed > HMD20_80_macs2_nodup_190_H3K4me3_common_rep1_rep2.narrowPeak.coverage.bed

awk '{if ($4<20) print $0}' HMD_macs2_nodup_190_H3K27me3_common_rep1_rep2.broadPeak.coverage.bed >HMD20_macs2_nodup_190_H3K27me3_common_rep1_rep2.broadPeak.coverage.bed
awk '{if ($4<20) print $0}' HMD_macs2_nodup_190_H3K4me3_common_rep1_rep2.narrowPeak.coverage.bed > HMD20_macs2_nodup_190_H3K4me3_common_rep1_rep2.narrowPeak.coverage.bed
```

### Various selection of peaks base on HMD

``` bash
#files with info about peaks

ls HMD80_macs2_nodup_190_H3K*me3*coverage.bed
HMD80_macs2_nodup_190_H3K27me3_common_rep1_rep2.broadPeak.coverage.bed
HMD80_macs2_nodup_190_H3K4me3_common_rep1_rep2.narrowPeak.coverage.bed
ann_dir='/Users/angelas/Documents/angelas/Documents/MAMI_Egg_extract_analysis'

#create also a set of peaks where the HMD is above the 80th qquantile
awk '{if ($4>=80.3101) print $0}' HMD_macs2_nodup_190_H3K4me3_common_rep1_rep2.narrowPeak.coverage.bed | bedtools sort -i -  > HMDquantile80_macs2_nodup_190_H3K4me3_common_rep1_rep2_noladder.narrowPeak.coverage.bed

awk '{if ($4<40.5648) print $0}' HMD_macs2_nodup_190_H3K4me3_common_rep1_rep2.narrowPeak.coverage.bed | bedtools sort -i -  > HMDbelowquantile20_macs2_nodup_190_H3K4me3_common_rep1_rep2_noladder.narrowPeak.coverage.bed

awk '{if ($4>=133.783) print $0}' HMD_macs2_nodup_190_H3K27me3_common_rep1_rep2.broadPeak.coverage.bed | bedtools sort -i -  > HMDquantile80_macs2_nodup_190_H3K27me3_common_rep1_rep2_noladder.broadPeak.coverage.bed

awk '{if ($4<74.3238) print $0}' HMD_macs2_nodup_190_H3K27me3_common_rep1_rep2.broadPeak.coverage.bed | bedtools sort -i -  > HMDbelowquantile20_macs2_nodup_190_H3K27me3_common_rep1_rep2_noladder.broadPeak.coverage.bed

bedtools intersect -a HMD80_macs2_nodup_190_H3K4me3_common_rep1_rep2_noladder.narrowPeak.coverage.bed -b $ann_dir/Xlaevis.TSS_only_genes_strand_bed6_slop_b2000.sorted.bed -wb | cut -f 8 | sort | uniq > HMD80_macs2_nodup_190_H3K4me3_common_rep1_rep2_noladder.narrowPeak.promoterb2000_in_region.bed

bedtools intersect -a HMD80_macs2_nodup_190_H3K27me3_common_rep1_rep2_noladder.broadPeak.coverage.bed -b $ann_dir/Xlaevis.TSS_only_genes_strand_bed6_slop_b2000.sorted.bed -wb | cut -f 8 | sort | uniq > HMD80_macs2_nodup_190_H3K27me3_common_rep1_rep2_noladder.broadPeak.promoterb2000_in_region.bed

bedtools intersect -a HMDquantile80_macs2_nodup_190_H3K4me3_common_rep1_rep2_noladder.narrowPeak.coverage.bed -b $ann_dir/Xlaevis.TSS_only_genes_strand_bed6_slop_b2000.sorted.bed -wb | cut -f 8 | sort | uniq > HMDquantile80_macs2_nodup_190_H3K4me3_common_rep1_rep2_noladder.narrowPeak.promoterb2000_in_region.bed

bedtools intersect -a HMDquantile80_macs2_nodup_190_H3K27me3_common_rep1_rep2_noladder.broadPeak.coverage.bed -b $ann_dir/Xlaevis.TSS_only_genes_strand_bed6_slop_b2000.sorted.bed -wb | cut -f 8 | sort | uniq > HMDquantile80_macs2_nodup_190_H3K27me3_common_rep1_rep2_noladder.broadPeak.promoterb2000_in_region.bed

bedtools intersect -a HMDbelowquantile20_macs2_nodup_190_H3K4me3_common_rep1_rep2_noladder.narrowPeak.coverage.bed -b $ann_dir/Xlaevis.TSS_only_genes_strand_bed6_slop_b2000.sorted.bed -wb | cut -f 8 | sort | uniq > HMDbelowquantile20_macs2_nodup_190_H3K4me3_common_rep1_rep2_noladder.narrowPeak.promoterb2000_in_region.bed

bedtools intersect -a HMDbelowquantile20_macs2_nodup_190_H3K27me3_common_rep1_rep2_noladder.broadPeak.coverage.bed -b $ann_dir/Xlaevis.TSS_only_genes_strand_bed6_slop_b2000.sorted.bed -wb | cut -f 8 | sort | uniq > HMDbelowquantile20_macs2_nodup_190_H3K27me3_common_rep1_rep2_noladder.broadPeak.promoterb2000_in_region.bed
```

### Check regions of the genome (size of those regions) with HMD above desidered threshold

``` bash
#check how much of the genome is > 100HMD, >80HMD, 20-80, and <20 
#define HMD_thr
HMD_thr=100

#count bases
grep -v 601_ HMD_${file}_mergedRep_nodup_Fragm190.50bpw.bed | awk '{if($4>=$HMD_thr) {sum100 +=($3-$2)}} END{print sum100}

#save regions
grep -v 601_C HMD_${file}_mergedRep_nodup_IPallr_Fragm190.50bpw.bed | awk '{if($4>=$HMD_thr) print $0}' > HMD${HMD_thr}_${file}_mergedRep_nodup_Fragm190.50bpw.bed
```
